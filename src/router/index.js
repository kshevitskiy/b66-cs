import Vue from 'vue'
import Router from 'vue-router'

import Main from '@/components/main/Main.vue'
import Home from '@/components/main/home/Home.vue'
import About from '@/components/main/about/About.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [      
    {
      path: '/',      
      component: Main,
      children: [
        {
          path: '/',
          name: 'home',
          component: Home,
        }, {
          path: '/about',
          name: 'about',
          component: About,
        }
      ],      
    },
    {
      path: '/*',
      redirect: '/'
    }
  ],
})