<?php // for MailChimp API v3.0

include('../vendor/drewm/mailchimp-api/src/MailChimp.php');  // path to API wrapper downloaded from GitHub

use \DrewM\MailChimp\MailChimp;

function storeAddress() {

    $key        = "95323fb4086f536e360d3e49ecba474f-us18";
    $list_id    = "91fc511199";

    // mailchimp fields
    $merge_vars = array(
        'FNAME' => $_POST['FNAME']
    );

    $mc = new MailChimp($key);

    // add the email to your list
    $result = $mc->post('/lists/'.$list_id.'/members', array(
            'email_address'     => $_POST['EMAIL'],
            'status'            => 'pending',
            'merge_fields'      => $merge_vars,
            'update_existing'   => true
        )
    );

    return json_encode($result);

}

// If being called via ajax, run the function, else fail
if ($_POST['FNAME'] && $_POST['EMAIL']) {
    echo storeAddress(); // send the response back through Ajax
} else {
    echo '{"detail": "One or more required fields are not completed"}';
}